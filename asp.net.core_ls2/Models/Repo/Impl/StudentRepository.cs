﻿using DataAccess;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asp.net.core_ls2.Models.Repo.Impl
{
    public class StudentRepository : IStudentRepository<Student>
    {
        StudentDbContext context;

        public StudentRepository(StudentDbContext c)
        {
            context = c;
        }
        public async void Create(Student student)
        {
            context.Set<Student>().Add(student);
            await context.SaveChangesAsync();

        }

        public void Delete(int id)
        {
            var student = context.Students.Where(x => x.Id == id).FirstOrDefault();
            context.Students.Remove(student);
        }

        public async Task Edit(int id, Student item)
        {
            var student = await context.Students.Where(x => x.Id == id).FirstOrDefaultAsync();
            context.Entry<Student>(item).State = EntityState.Modified;
        }

        public async Task <IList<Student>> GetAll()
        {
            return await context.Students.ToListAsync();
        }

        public async Task<Student> GetOne(int id)
        {
            return await context.Students.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

    }
}
