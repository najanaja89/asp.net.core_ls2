﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asp.net.core_ls2.Models.Repo
{
    public interface IStudentRepository<T> where T : class
    {
        void Create(T item);
        Task Edit(int id, T item);

        Task<T> GetOne(int id);

        Task<IList<T>> GetAll();

        void Delete(int id);
    }
}
